// CHECK IF USER HAS A MICROPHONE
function hasGetUserMedia() {
    return !!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia);
}

if (hasGetUserMedia()) {
    // Good to go!
} else {
    alert('getUserMedia() is not supported by your browser');
}


// Getting the Audio Record Button
var record_audio_btn = document.getElementById('record-audio-btn');

// If someone presses the Audio Record Button start the Function 'record_audio'
recording = false;
record_audio_btn.addEventListener('click', record_audio);

// Set global variables (not recommended, but I can't be bothered honestly...)
var mediaRecorder;
// This is where we will save the Audio 'Chunks' because 'mediaDevices' can't 
// buffer a lot of Data on it's own
var chunks;

soundClips = document.getElementById('sound_clips');

function record_audio() {
    if (recording == false) {
        recording = true;
        

        // Get access to the Microfone, as soon as accepted record and write all the 'chunks' into the
        // 'chunks' array
        navigator.mediaDevices.getUserMedia({ audio: true }).then(function (stream) {
            mediaRecorder = new MediaRecorder(stream);

            mediaRecorder.start();
            console.log(mediaRecorder.state);
            console.log('recorder started');
            record_audio_btn.style.background = 'rgb(40,167,69)';
            record_audio_btn.style.color = 'white';


            mediaRecorder.ondataavailable = function (e) {
                chunks.push(e.data);
            };
        })

        // If something went wrong, like the User said he doesn't want to be recorded
        .catch(function (err) {
            console.log('The following error occurred: ' + err);
        });
    } else {

        // When the button is pressed again and the user wants to stop the recording
        mediaRecorder.stop();
        console.log(mediaRecorder.state);
        console.log('recorder stopped');
        record_audio_btn.style.background = '';
        record_audio_btn.style.color = '';

        console.log('data available after MediaRecorder.stop() called.');

        // The finished Audio File in .ogg format!
        var audioFile = new Blob(chunks, { 'type' : 'audio/ogg; codecs=opus' });
        chunks = [];
        var audioURL = URL.createObjectURL(audioFile);
        console.log(audioURL);
        console.log('recorder stopped');
        send_msg(audio=true);
        recording = false;
    }
}



// CREATE A CHAT BUBBLE
messages = document.getElementById('messages');
send_btn = document.getElementById('send-btn');
input_text = document.getElementById('input-text');



send_btn.addEventListener('click', send_msg);
input_text.addEventListener('keyup', function (e) {
    var key = e.which || e.keyCode;
    if (key === 13) { // 13 is enter
        send_msg();
    }
});

function send_msg(audio = false){
    if (audio == true){
        // TODO: replace with actuall Audio File, and let it be playable
        var container = document.createElement('div');
        var inside = document.createElement('div');
        var meta_info = document.createElement('span');
        
        container.classList.add('d-flex', 'justify-content-end');
        inside.classList.add('msg_cotainer_send');
        inside.innerHTML = "Your Audio Message!";
        meta_info.classList.add('msg_time_send');

        // Getting the Time and Day

        var options = {  weekday: 'long', hour: '2-digit', minute: '2-digit', hour12: false };
        var prnDt = new Date().toLocaleTimeString('en-us', options);

        console.log(prnDt);

        meta_info.innerHTML = prnDt;

        inside.appendChild(meta_info);
        container.appendChild(inside);
        messages.appendChild(container);
    }
    else if (input_text.value.length != 0){
        var container = document.createElement('div');
        var inside = document.createElement('div');
        var meta_info = document.createElement('span');
        
        container.classList.add('d-flex', 'justify-content-end');
        inside.classList.add('msg_cotainer_send');
        inside.innerHTML = input_text.value;
        meta_info.classList.add('msg_time_send');

        // Getting the Time and Day

        var options = {  weekday: 'long', hour: '2-digit', minute: '2-digit', hour12: true };
        var prnDt = new Date().toLocaleTimeString('en-us', options);
        

        meta_info.innerHTML = prnDt;

        inside.appendChild(meta_info);
        container.appendChild(inside);
        messages.appendChild(container);
    }
}
